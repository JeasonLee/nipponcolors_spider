import urllib.request
import ssl


class HTMLDownLoader(object):
    def download(self, url):
        if url is None:
            return None
        print(url)
        context = ssl._create_unverified_context()
        resposen = urllib.request.urlopen(url, context=context)
        if resposen.getcode() != 200:
            return None
        print('抓取完成')
        return resposen.read()