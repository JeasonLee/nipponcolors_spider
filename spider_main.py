# -*- coding: utf-8 -*-

import html_downloader, html_outputer, html_parser
import json


class SpiderMain(object):
    def __init__(self):
        self.downloader = html_downloader.HTMLDownLoader()
        self.parser = html_parser.HTMLParser()
        self.outputer = html_outputer.HTMLOutputer()

    # -------------- wiki --------------#

    # 爬取颜色
    def craw_wiki(self):
        wiki_url = "https://en.wikipedia.org/wiki/Traditional_colors_of_Japan"
        html_code = self.downloader.download(wiki_url)
        items = self.parser.parse_wiki(html_code)
        self.outputer.output_json(items,'nippon_color_wiki.json')

    # -------------- colordic --------------#

    #爬取颜色
    def craw_colordic(self):
        colordic_url = "http://www.colordic.org/w/"
        html_code = self.downloader.download(colordic_url)
        items = self.parser.parse_colordic(html_code)
        self.outputer.output_json(items,'nippon_color_colordic.json')

    # -------------- irocore.com --------------#
    
    # 爬取颜色基础信息&颜色详情链接
    def craw_irocore_link(self):
        irocore_url = 'http://irocore.com'
        html_code = self.downloader.download(irocore_url)
        category_urls = self.parser.parse_irocore_category_link(html_code)
        category_urls.pop()
        link_items = list()
        list_items = list()
        for url in category_urls:
            html_code = self.downloader.download(url)
            link_items.extend(self.parser.parse_irocore_info_link(html_code))
            list_items.extend(self.parser.parse_irocore_list(html_code))
        self.outputer.output_json(list_items, 'nippon_color_irocore_list.json')
        self.outputer.output_json(link_items, 'nippon_color_irocore_info_link.json')

    # 爬取颜色详情
    def craw_irocore_info(self):
        info_items = list()
        # 读取info连接
        file = open('nippon_color_irocore_info_link.json', encoding='utf-8')
        links = json.load(file)
        for url in links:
            html_code = self.downloader.download(url)
            info_items.append(self.parser.parse_irocore_info(html_code))
            self.outputer.output_json(info_items, 'nippon_color_irocore_infos.json')

    # 检查爬取的数据
    def check_irocore(self):
        # 列表
        file1 = open('nippon_color_irocore_list.json', encoding='utf-8')
        items = json.load(file1)
        names1 = list()
        for item in items:
            if '#' not in item['hexTriplet']:
                names1.append(item['romanized'])
        print(names1)

        # 详情
        file2 = open('nippon_color_irocore_infos.json', encoding='utf-8')
        items = json.load(file2)
        names2 = list()
        copy_items = items
        for item in items:
            if item['content_jp'] == '':
                names2.append(item['romanized'])
                copy_items.remove(item)
        print(names2)
        nocontent_links = ['http://irocore.com/' + name for name in names2]
        self.outputer.output_json(nocontent_links, 'nippon_color_irocore_info_link_nocontent.json')
        self.outputer.output_json(copy_items, 'nippon_color_irocore_infos.json')

if __name__ == "__main__":
    object_spider = SpiderMain()
    object_spider.craw_wiki()

    object_spider.craw_colordic()

    object_spider.craw_irocore_link()
    # object_spider.craw_irocore_info()
    # object_spider.check_irocore()
