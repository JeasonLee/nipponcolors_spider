import json
import sqlite3

class HTMLOutputer(object):
    def output_json(self, items, file_name):
        jsonString = json.dumps(items, indent=4, sort_keys=False, ensure_ascii=False)
        # print(jsonString)
        file = open(file_name, 'w')
        file.write(jsonString)
        file.close()
        print(file_name + '输出完成')


