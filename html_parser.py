from bs4 import BeautifulSoup


class HTMLParser(object):

# -------------- wiki --------------#

    def parse_wiki(self, html_code):
        if html_code is None:
            return None
        soup = BeautifulSoup(html_code, 'html.parser', from_encoding='utf-8')
        html_datas = soup.find_all('table', class_='wikitable')

        items = []
        item_names = ['name', 'romanized', 'englishTranslation', 'RGB', 'hexTriplet']

        for item_html_datas in html_datas:
            item_datas = item_html_datas.find_all('td')

            itemDics = list()
            itemDic = dict()

            for i in range(0, len(item_datas)):
                j = i % 5
                item = item_datas[i]
                itemDic[item_names[j]] = item.get_text()
                if len(itemDic) == 5:
                    itemDics.append(itemDic)
                    itemDic = dict()
            items.extend(itemDics)
        print('解析完成')
        return items

# -------------- colordic --------------#

    def parse_colordic(self, html_code):
        if html_code is None:
            return None
        soup = BeautifulSoup(html_code, 'html.parser', from_encoding='utf-8')
        html_datas = soup.find_all('table', class_='colortable')
        items = [[], [], [], [], []]
        for items_html_datas in html_datas:
            items_datas = items_html_datas.find_all('td')
            for i in range(0, len(items_datas)):
                item = items_datas[i]
                text_color_string = item.find('a').attrs['style']
                nippon_name = item.find('span').get_text()
                strings = item.get_text().split(nippon_name)
                item_dic = dict()
                item_dic['name'] = strings[0]
                item_dic['nipponName'] = nippon_name
                item_dic['hexTriplet'] = strings[1]
                item_dic['textColor'] = text_color_string.split(':')[1]
                j = i % 5
                items[j].append(item_dic)
        result_items = list()
        for item in items:
            result_items.extend(item)
        print('解析完成')
        return result_items

# -------------- irocore.com --------------#

    def parse_irocore_category_link(self, html_code):
        if html_code is None:
            return None
        soup = BeautifulSoup(html_code, 'html.parser', from_encoding='utf-8')
        html_datas = soup.find('div', id='side').find('aside', class_='widget').find_all('a')
        items = [item.get('href') for item in html_datas]
        print(items)
        return items

    def parse_irocore_info_link(self, html_code):
        if html_code is None:
            return None
        soup = BeautifulSoup(html_code, 'html.parser', from_encoding='utf-8')
        html_datas = soup.find('article', class_='hentry').find('div', class_='news10').find_all('a')
        items = [item.get('href') for item in html_datas]
        print(items)
        return items

    def parse_irocore_list(self, html_code):
        if html_code is None:
            return None
        soup = BeautifulSoup(html_code, 'html.parser', from_encoding='utf-8')
        html_datas = soup.find('article', class_='hentry').find('div', class_='news10').find_all('img')
        items = [self.__parser_irocore_list(item) for item in html_datas];
        print(items)
        return items;

    def __parser_irocore_list(self, item):
        item_dic = dict()
        string = item.get('alt')
        if '-iro' in string:
            new_string = string.replace('-iro', '$$iro')
            strings = new_string.replace('）', '').replace('（', '-').split('-')
            strings[1] = strings[1].replace('$$iro', '-iro')
        else:
            strings = string.replace('）', '').replace('（', '-').split('-')
        item_dic['name'] = strings[0]
        item_dic['romanized'] = strings[1]
        item_dic['hexTriplet'] = strings[2]
        return item_dic

    def parse_irocore_info(self, html_code):
        if html_code is None:
            return None
        soup = BeautifulSoup(html_code, 'html.parser', from_encoding='utf-8')
        [s.extract() for s in soup('section')]  # 删除注意事项
        html_datas = soup.find('div', class_='entry-content')
        table_datas = html_datas.find('table', class_='tables').find_all('td')
        table_items = [item.get_text() for item in table_datas]
        content_datas = html_datas.find_all('p')
        content_items = [item.get_text() for item in content_datas]

        item_dic = dict()
        name_strings = table_items[0].replace('）', '').split('（')
        item_dic['name'] = name_strings[0]
        item_dic['nipponName'] = name_strings[1]
        item_dic['romanized'] = table_items[1]
        item_dic['hexTriplet'] = table_items[4]

        rgb_dic = dict()
        rgb_strings = table_items[2].strip().split(' ')
        for rgb_string in rgb_strings:
            strings = rgb_string.split(':')
            rgb_dic[strings[0].lower()] = strings[1]
        item_dic['rgb'] = rgb_dic

        cmyk_dic = dict()
        cmyk_strings = table_items[3].strip().split(' ')
        for cmyk_string in cmyk_strings:
            strings = cmyk_string.split(':')
            cmyk_dic[strings[0].lower()] = strings[1]
        item_dic['cmyk'] = cmyk_dic

        read_jp = '-読み：' + item_dic['nipponName'] + '-'
        read_en = '-read: ' + item_dic['romanized'] + '-'
        content_strings = ''.join(content_items).split(read_jp)
        if(len(content_strings) == 2):
            item_dic['content_jp'] = content_strings[0]
            item_dic['content_en'] = content_strings[1].strip().replace(read_en, '')  # 去除前后空格 并移除read_en
        else:
            item_dic['content_jp'] = content_strings[0]
            item_dic['content_en'] = ''
        print(item_dic)
        return item_dic;




